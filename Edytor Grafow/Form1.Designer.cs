﻿
namespace Edytor_Grafow
{
    partial class Edytor_grafow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Edytor_grafow));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.Edycja_Group = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.Kolor_Button = new System.Windows.Forms.Button();
            this.Kolor_PictureBox = new System.Windows.Forms.Label();
            this.Wyczysc_Button = new System.Windows.Forms.Button();
            this.Usun_Button = new System.Windows.Forms.Button();
            this.Jezyk_Group = new System.Windows.Forms.GroupBox();
            this.Angielski_Button = new System.Windows.Forms.Button();
            this.Polski_Button = new System.Windows.Forms.Button();
            this.Import_Eksport_Group = new System.Windows.Forms.GroupBox();
            this.Wczytaj_Button = new System.Windows.Forms.Button();
            this.Zapisz_Button = new System.Windows.Forms.Button();
            this.ObszarRoboczy_PictureBox = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.Edycja_Group.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.Jezyk_Group.SuspendLayout();
            this.Import_Eksport_Group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ObszarRoboczy_PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.ObszarRoboczy_PictureBox, 0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.Edycja_Group, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.Jezyk_Group, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.Import_Eksport_Group, 0, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            // 
            // Edycja_Group
            // 
            resources.ApplyResources(this.Edycja_Group, "Edycja_Group");
            this.Edycja_Group.Controls.Add(this.tableLayoutPanel3);
            this.Edycja_Group.Controls.Add(this.Wyczysc_Button);
            this.Edycja_Group.Controls.Add(this.Usun_Button);
            this.Edycja_Group.Name = "Edycja_Group";
            this.Edycja_Group.TabStop = false;
            // 
            // tableLayoutPanel3
            // 
            resources.ApplyResources(this.tableLayoutPanel3, "tableLayoutPanel3");
            this.tableLayoutPanel3.Controls.Add(this.Kolor_Button, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.Kolor_PictureBox, 1, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            // 
            // Kolor_Button
            // 
            resources.ApplyResources(this.Kolor_Button, "Kolor_Button");
            this.Kolor_Button.Name = "Kolor_Button";
            this.Kolor_Button.UseVisualStyleBackColor = true;
            this.Kolor_Button.Click += new System.EventHandler(this.Kolor_Button_Click);
            // 
            // Kolor_PictureBox
            // 
            resources.ApplyResources(this.Kolor_PictureBox, "Kolor_PictureBox");
            this.Kolor_PictureBox.BackColor = System.Drawing.Color.Black;
            this.Kolor_PictureBox.Name = "Kolor_PictureBox";
            // 
            // Wyczysc_Button
            // 
            resources.ApplyResources(this.Wyczysc_Button, "Wyczysc_Button");
            this.Wyczysc_Button.Name = "Wyczysc_Button";
            this.Wyczysc_Button.UseVisualStyleBackColor = true;
            this.Wyczysc_Button.Click += new System.EventHandler(this.Wyczysc_Button_Click);
            // 
            // Usun_Button
            // 
            resources.ApplyResources(this.Usun_Button, "Usun_Button");
            this.Usun_Button.Name = "Usun_Button";
            this.Usun_Button.UseVisualStyleBackColor = true;
            this.Usun_Button.Click += new System.EventHandler(this.Usun_Button_Click);
            // 
            // Jezyk_Group
            // 
            this.Jezyk_Group.Controls.Add(this.Angielski_Button);
            this.Jezyk_Group.Controls.Add(this.Polski_Button);
            resources.ApplyResources(this.Jezyk_Group, "Jezyk_Group");
            this.Jezyk_Group.Name = "Jezyk_Group";
            this.Jezyk_Group.TabStop = false;
            // 
            // Angielski_Button
            // 
            resources.ApplyResources(this.Angielski_Button, "Angielski_Button");
            this.Angielski_Button.Name = "Angielski_Button";
            this.Angielski_Button.UseVisualStyleBackColor = true;
            this.Angielski_Button.Click += new System.EventHandler(this.Angielski_Button_Click);
            // 
            // Polski_Button
            // 
            resources.ApplyResources(this.Polski_Button, "Polski_Button");
            this.Polski_Button.Name = "Polski_Button";
            this.Polski_Button.UseVisualStyleBackColor = true;
            this.Polski_Button.Click += new System.EventHandler(this.Polski_Button_Click);
            // 
            // Import_Eksport_Group
            // 
            resources.ApplyResources(this.Import_Eksport_Group, "Import_Eksport_Group");
            this.Import_Eksport_Group.Controls.Add(this.Wczytaj_Button);
            this.Import_Eksport_Group.Controls.Add(this.Zapisz_Button);
            this.Import_Eksport_Group.Name = "Import_Eksport_Group";
            this.Import_Eksport_Group.TabStop = false;
            // 
            // Wczytaj_Button
            // 
            resources.ApplyResources(this.Wczytaj_Button, "Wczytaj_Button");
            this.Wczytaj_Button.Name = "Wczytaj_Button";
            this.Wczytaj_Button.UseVisualStyleBackColor = true;
            this.Wczytaj_Button.Click += new System.EventHandler(this.Wczytaj_Button_Click);
            // 
            // Zapisz_Button
            // 
            resources.ApplyResources(this.Zapisz_Button, "Zapisz_Button");
            this.Zapisz_Button.Name = "Zapisz_Button";
            this.Zapisz_Button.UseVisualStyleBackColor = true;
            this.Zapisz_Button.Click += new System.EventHandler(this.Zapisz_Button_Click);
            // 
            // ObszarRoboczy_PictureBox
            // 
            resources.ApplyResources(this.ObszarRoboczy_PictureBox, "ObszarRoboczy_PictureBox");
            this.ObszarRoboczy_PictureBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ObszarRoboczy_PictureBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.ObszarRoboczy_PictureBox.Name = "ObszarRoboczy_PictureBox";
            this.ObszarRoboczy_PictureBox.TabStop = false;
            this.ObszarRoboczy_PictureBox.SizeChanged += new System.EventHandler(this.ObszarRoboczy_PictureBox_SizeChanged);
            this.ObszarRoboczy_PictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ObszarRoboczy_PictureBox_MouseDown);
            this.ObszarRoboczy_PictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ObszarRoboczy_PictureBox_MouseMove);
            this.ObszarRoboczy_PictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ObszarRoboczy_PictureBox_MouseUp);
            // 
            // Edytor_grafow
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.Name = "Edytor_grafow";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Edytor_grafow_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Edytor_grafow_KeyDown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.Edycja_Group.ResumeLayout(false);
            this.Edycja_Group.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.Jezyk_Group.ResumeLayout(false);
            this.Import_Eksport_Group.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ObszarRoboczy_PictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox Edycja_Group;
        private System.Windows.Forms.Button Kolor_Button;
        private System.Windows.Forms.GroupBox Jezyk_Group;
        private System.Windows.Forms.Button Angielski_Button;
        private System.Windows.Forms.Button Polski_Button;
        private System.Windows.Forms.GroupBox Import_Eksport_Group;
        private System.Windows.Forms.Button Wczytaj_Button;
        private System.Windows.Forms.Button Zapisz_Button;
        private System.Windows.Forms.PictureBox ObszarRoboczy_PictureBox;
        private System.Windows.Forms.Button Wyczysc_Button;
        private System.Windows.Forms.Button Usun_Button;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label Kolor_PictureBox;
    }
}

