﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Globalization;
using System.Collections.Generic;

namespace Edytor_Grafow
{

    public partial class Edytor_grafow : Form
    {
        //Dane 
        public class Data
        {
            public int X;
            public int Y;
            public Color _Color;
            public List<int> Edges;
            public Data(int X, int Y, Color c)
            {
                this.X = X;
                this.Y = Y;
                this._Color = c;
                this.Edges = new List<int>();
            }
        }
        //Bitmapy
        private Bitmap Canvas;
        private Bitmap Color_Field;

        //Stale 
        private const int Pen_width = 3;
        private const int radius = 15;

        //Patterny 
        private static readonly float[] pattern = { 1.0F, 1.0F };
        private static readonly float[] common = { 1.0F };

        //Format Napisu
        private static readonly StringFormat format = new StringFormat()
        {
            LineAlignment = StringAlignment.Center,
            Alignment = StringAlignment.Center
        };

        //Zmienne do Interface'u
        private Pen _pen = new Pen(Color.Black, Pen_width);

        //Zasoby
        private readonly ComponentResourceManager resources;

        //Zmienne do Logiki programu 
        private static int couter = 1;
        private List<Data> Vertex = new List<Data>();
        private int marked = -1;
        private int mouse_x = 0;
        private int mouse_y = 0;

        //Inicjacja
        public Edytor_grafow()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("pl-PL");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("pl-PL");
            resources = new ComponentResourceManager(typeof(Edytor_grafow));
            InitializeComponent();
            Canvas = new Bitmap(ObszarRoboczy_PictureBox.Size.Width, ObszarRoboczy_PictureBox.Size.Height);
            Color_Field = new Bitmap(Kolor_PictureBox.Size.Width, Kolor_PictureBox.Size.Height);
            ObszarRoboczy_PictureBox.Image = Canvas;
            Kolor_PictureBox.Image = Color_Field;
            KeyPreview = true;
            Kolor_Button.Focus();
        }

        //Przyciski
        private void Kolor_Button_Click(object sender, EventArgs e) {
            ColorDialog colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
                ChangeColor(colorDialog.Color);
        }
        private void Zapisz_Button_Click(object sender, EventArgs e)
        {
            SaveFileDialog saver = new SaveFileDialog();
            saver.Filter = "graph files (*.graph)|*.graph";
            if (saver.ShowDialog() == DialogResult.OK)
            {
                StreamWriter SW = null;
                try
                {
                    SW = File.CreateText(saver.FileName);
                    foreach (Data wd in Vertex)
                    {
                        SW.WriteLine(wd.X.ToString());
                        SW.WriteLine(wd.Y.ToString());
                        SW.WriteLine(wd._Color.ToArgb());
                    }
                    SW.WriteLine(" ");
                    for (int i = 0; i < Vertex.Count; i++)
                    {
                        for (int j = 0; j < Vertex[i].Edges.Count; j++)
                            SW.WriteLine(Vertex[i].Edges[j]);
                        SW.WriteLine(" ");
                    }
                }
                catch
                {
                    MessageBox.Show(resources.GetString("Blad_IO"));
                }
                finally
                {
                    SW.Close();
                }
                MessageBox.Show(resources.GetString("Sukces_Zapisu"));
            }
        }

        private void Wczytaj_Button_Click(object sender, EventArgs e)
        {
            OpenFileDialog saver = new OpenFileDialog();
            saver.Filter = "graph files (*.graph)|*.graph";
            if (saver.ShowDialog() == DialogResult.OK)
            {
                StreamReader SW = null;
                try
                {
                    SW = new StreamReader(saver.OpenFile());
                    if (LoadGraph(SW) == -1) MessageBox.Show(resources.GetString("Blad_IO"));
                }
                catch
                {
                    MessageBox.Show(resources.GetString("Blad_IO"));
                    return;
                }
                finally
                {
                    SW.Close();
                }
                MessageBox.Show(resources.GetString("Sukces_Zaladowania"));
                Usun_Button.Enabled = false;
                couter = Vertex.Count + 1;
                marked = -1;
                mouse_x = 0;
                mouse_y = 0;
                Canvas = new Bitmap(ObszarRoboczy_PictureBox.Size.Width, ObszarRoboczy_PictureBox.Size.Height);
                ObszarRoboczy_PictureBox.Image = Canvas;
                Draw_All();
            }
        }
        private void Wyczysc_Button_Click(object sender, EventArgs e) {
            Remove_all();
        }


        private void Usun_Button_Click(object sender, EventArgs e) {
            remove_marked();
            Usun_Button.Enabled = false;
        }

        private void Angielski_Button_Click(object sender, EventArgs e)
        {
            Set("en-GB");
            Draw_All();
        }

        private void Polski_Button_Click(object sender, EventArgs e)
        {
            Set("pl-PL");
            Draw_All();
        }
        //Eventy 
        
        //Klawiatura
        private void Edytor_grafow_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Delete)
            {
                if (marked != -1) remove_marked();
                e.Handled = true;
            }
        }
        //Inne
        private void ObszarRoboczy_PictureBox_SizeChanged(object sender, EventArgs e) {
            Canvas = new Bitmap(ObszarRoboczy_PictureBox.Size.Width, ObszarRoboczy_PictureBox.Size.Height);
            ObszarRoboczy_PictureBox.Image = Canvas;
            Draw_All();

        }
        private void Edytor_grafow_Paint(object sender, PaintEventArgs e)
        {
            Draw_All();
        }
        //Myszka   
        private void ObszarRoboczy_PictureBox_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left)
            {
                if (marked == -1)
                {
                    bool Za_blisko = false;
                    for (int i = 0; i < Vertex.Count; i++)
                    {
                        if (Math.Pow(e.X - Vertex[i].X, 2) + Math.Pow(e.Y - Vertex[i].Y, 2) < Math.Pow(2 * radius, 2) + 2 * Pen_width)
                        {
                            Za_blisko = true;
                            break;
                        }
                    }
                    if (!Za_blisko)
                    {
                        DrawVertex(e.X, e.Y, couter, _pen.Color);
                        couter++;
                        Vertex.Add(new Data(e.X, e.Y, _pen.Color));
                        ObszarRoboczy_PictureBox.Invalidate(new Rectangle(e.X - radius - Pen_width, e.Y - radius - Pen_width, (radius + Pen_width) * 2, (radius + Pen_width) * 2));
                    }
                }
                else
                {
                    int wie = Find_vertex(e.X, e.Y);
                    if (wie != marked && wie != -1)
                    {
                        if (!Vertex[marked].Edges.Contains(wie))
                        {
                            DrawLine(Vertex[wie].X, Vertex[wie].Y, Vertex[marked].X, Vertex[marked].Y, Color.Black);
                            Vertex[marked].Edges.Add(wie);
                            Vertex[wie].Edges.Add(marked);
                        }
                        else
                        {
                            DrawLine(Vertex[wie].X, Vertex[wie].Y, Vertex[marked].X, Vertex[marked].Y, Color.White);
                            Vertex[marked].Edges.Remove(wie);
                            Vertex[wie].Edges.Remove(marked);
                        }
                        Draw_All();
                    }
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                int wie = Find_vertex(e.X, e.Y);
                if (wie != -1 && marked == -1)
                {
                    marked = wie;
                    Draw_All();
                }
                else if (wie == -1 && marked != -1)
                {
                    marked = -1;
                    Usun_Button.Enabled = false;
                    Draw_All();
                }
                else if (wie != -1 && marked != -1)
                {
                    marked = wie;
                    Draw_All();
                }
            }

        }


        private void ObszarRoboczy_PictureBox_MouseMove(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Middle && marked != -1)
            {
                int move_X = e.X - mouse_x;
                int move_Y = e.Y - mouse_y;
                Color c = _pen.Color;
                DrawVertex(Vertex[marked].X, Vertex[marked].Y, marked + 1, Color.White);
                for (int i = 0; i < Vertex[marked].Edges.Count; i++)
                    DrawLine(Vertex[marked].X, Vertex[marked].Y, Vertex[Vertex[marked].Edges[i]].X, Vertex[Vertex[marked].Edges[i]].Y, Color.White);
                Vertex[marked].X += move_X;
                Vertex[marked].Y += move_Y;
                _pen.Color = c;
                Draw_All();
            }
            mouse_x = e.X;
            mouse_y = e.Y;
        }

        private void ObszarRoboczy_PictureBox_MouseUp(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Middle && marked != -1)
            {
                Color c = _pen.Color;
                DrawVertex(Vertex[marked].X, Vertex[marked].Y, marked + 1, Color.White);
                for (int i = 0; i < Vertex[marked].Edges.Count; i++)
                    DrawLine(Vertex[marked].X, Vertex[marked].Y, Vertex[Vertex[marked].Edges[i]].X, Vertex[Vertex[marked].Edges[i]].Y, Color.White);
                if (Vertex[marked].X < 0) Vertex[marked].X = 0;
                if (Vertex[marked].Y < 0) Vertex[marked].Y = 0;
                if (Vertex[marked].X > ObszarRoboczy_PictureBox.Width) Vertex[marked].X = ObszarRoboczy_PictureBox.Width;
                if (Vertex[marked].Y > ObszarRoboczy_PictureBox.Height) Vertex[marked].Y = ObszarRoboczy_PictureBox.Height;
                _pen.Color = c;
                Draw_All();
            }
            mouse_x = e.X;
            mouse_y = e.Y;
        }



        //Funckje Pomocnicze
        private void DrawVertex(int X, int Y, int i, Color c)
        {
            using (Graphics g = Graphics.FromImage(Canvas))
            {
                _pen.Color = c;
                g.FillEllipse(Brushes.White, X - radius, Y - radius, radius * 2, radius * 2);
                g.DrawEllipse(_pen, X - radius, Y - radius, radius * 2, radius * 2);
                g.DrawString((i).ToString(), this.Font, _pen.Brush, X, Y, format);
            }
        }

        private void DrawLine(int X1, int Y1, int X2, int Y2, Color c)
        {
            using (Graphics g = Graphics.FromImage(Canvas))
            {
                if (c == Color.White) _pen.Width++;
                Color kolor = _pen.Color;
                _pen.Color = c;
                g.DrawLine(_pen, X1, Y1, X2, Y2);
                _pen.Color = kolor;
                if (c == Color.White) _pen.Width--;
            }
        }

        private void Mark(int X, int Y, int wie)
        {
            Usun_Button.Enabled = true;
            Color c = _pen.Color;
            DrawVertex(X, Y, wie + 1, Color.White);
            _pen.DashPattern = pattern;
            DrawVertex(X, Y, wie + 1, Vertex[wie]._Color);
            _pen.Color = c;
            _pen.DashPattern = common;
        }


        private void remove_marked()
        {
            Color c = _pen.Color;
            DrawVertex(Vertex[marked].X, Vertex[marked].Y, marked + 1, Color.White);
            for (int j = Vertex[marked].Edges.Count - 1; j >= 0; j--)
            {
                DrawLine(Vertex[Vertex[marked].Edges[j]].X, Vertex[Vertex[marked].Edges[j]].Y, Vertex[marked].X, Vertex[marked].Y, Color.White);
                Vertex[Vertex[marked].Edges[j]].Edges.Remove(marked);
                Vertex[marked].Edges.Remove(Vertex[marked].Edges[j]);
            }
            for (int i = marked; i < Vertex.Count - 1; i++)
            {
                Vertex[i]._Color = Vertex[i + 1]._Color;
                Vertex[i].X = Vertex[i + 1].X;
                Vertex[i].Y = Vertex[i + 1].Y;
                Vertex[i].Edges = Vertex[i + 1].Edges;
            }
            Vertex.RemoveAt(Vertex.Count - 1);
            for (int i = 0; i < Vertex.Count; i++)
                for (int j = 0; j < Vertex[i].Edges.Count; j++)
                    if (Vertex[i].Edges[j] > marked) Vertex[i].Edges[j]--;
            marked = -1;
            Usun_Button.Enabled = false;
            couter--;
            _pen.Color = c;
            Draw_All();
        }

        private int Find_vertex(int X, int Y)
        {
            int wie = -1;
            double naj_odl = double.MaxValue;
            for (int i = 0; i < Vertex.Count; i++)
            {
                double odleglosc = Math.Pow(X - Vertex[i].X, 2) + Math.Pow(Y - Vertex[i].Y, 2);
                if (odleglosc < Math.Pow(radius + Pen_width, 2) && naj_odl > odleglosc)
                {
                    wie = i;
                    naj_odl = odleglosc;
                }
            }
            return wie;
        }

        private void Draw_All()
        {
            for (int i = 0; i < Vertex.Count; i++)
                for (int j = 0; j < Vertex[i].Edges.Count; j++)
                    DrawLine(Vertex[Vertex[i].Edges[j]].X, Vertex[Vertex[i].Edges[j]].Y, Vertex[i].X, Vertex[i].Y, Color.Black);
            Color c = _pen.Color;
            for (int i = 0; i < Vertex.Count; i++)
            {
                DrawVertex(Vertex[i].X, Vertex[i].Y, i + 1, Vertex[i]._Color);
                if (marked == i)
                    Mark(Vertex[marked].X, Vertex[marked].Y, marked);
            }
            _pen.Color = c;
            ObszarRoboczy_PictureBox.Refresh();
        }
        private void ChangeColor(Color kolor)
        {
            Kolor_PictureBox.BackColor = kolor;
            _pen.Color = kolor;
            Kolor_PictureBox.Refresh();
            if (marked != -1)
            {
                Vertex[marked]._Color = kolor;
                Draw_All();
            }

        }

        private void Remove_all()
        {
            Color kolor = _pen.Color;
            couter = 1;
            for (int i = Vertex.Count - 1; i >= 0; i--)
            {
                DrawVertex(Vertex[i].X, Vertex[i].Y, i + 1, Color.White);
                for (int j = Vertex[i].Edges.Count - 1; j >= 0; j--)
                {
                    DrawLine(Vertex[Vertex[i].Edges[j]].X, Vertex[Vertex[i].Edges[j]].Y, Vertex[i].X, Vertex[i].Y, Color.White);
                    Vertex[Vertex[i].Edges[j]].Edges.Remove(i);
                    Vertex[i].Edges.Remove(Vertex[i].Edges[j]);
                }
                Vertex.RemoveAt(i);
            }
            Usun_Button.Enabled = false;
            marked = -1;
            ObszarRoboczy_PictureBox.Refresh();
            _pen.Color = kolor;
        }

        private int LoadGraph(StreamReader stream)
        {
            var WierzcholkiNEW = new List<Data>();
            int vertex_counter = 0;
            while (!stream.EndOfStream && stream.Peek() != ' ')
            {
                vertex_counter++;
                var X = stream.ReadLine();
                var Y = stream.ReadLine();
                var c = stream.ReadLine();
                WierzcholkiNEW.Add(new Data(int.Parse(X), int.Parse(Y), Color.FromArgb(int.Parse(c))));
            }
            if (stream.Peek() == ' ') stream.ReadLine();
            int counter = 0;
            while (!stream.EndOfStream)
            {
                if (stream.Peek() == ' ') { counter++; stream.ReadLine(); }
                else
                {
                    var wsp = stream.ReadLine();
                    int wsp_ = int.Parse(wsp);
                    if (wsp_ > vertex_counter - 1) return -1;
                    if (counter < vertex_counter) WierzcholkiNEW[counter].Edges.Add(wsp_);
                    else return -1;
                }
            }
            Remove_all();
            Vertex = WierzcholkiNEW;
            return 0;
        }

        public void Set(string language)
        { 
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(language);
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(language);
            var oldSize = Size;
            var loc = Location;
            Color c = Kolor_PictureBox.BackColor;

            Controls.Clear();
            InitializeComponent();

            ObszarRoboczy_PictureBox.Image = Canvas;
            Kolor_PictureBox.BackColor = c;
            Size = oldSize;
            Location = loc;

            KeyPreview = true;
            if (marked != -1)
                Usun_Button.Enabled = true;
            Kolor_Button.Focus();
        }
    }
}
